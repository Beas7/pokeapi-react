import React, { useState } from 'react';
import { BrowserRouter, Navigate, Routes, Route } from 'react-router-dom';
import styled from 'styled-components';
import Home from './components/home/home';
import Nav from './components/nav/nav';
import PokemonDetail from './components/pokemonDetail/pokemonDetail';
import Pokemon from './components/pokedex/pokemon';

const ContainerFluid = styled.section`
  padding-top: 80px;
`;

export default function App() {
  const [pkmnTeam, setPkmnTeam] = useState([]);

  return (
    <BrowserRouter>
      <Nav />
      <ContainerFluid>
        <Routes>
          <Route exact path='/pokemon' element={<Pokemon pkmnTeam={pkmnTeam} setPkmnTeam={setPkmnTeam} />} />
          <Route exact path='/pokemon/:pokemonName' element={<PokemonDetail />} />
          <Route exact path='/' element={<Home pkmnTeam={pkmnTeam} />} />
          <Route path='*' element={<Navigate to='/' replace />} />
        </Routes>
      </ContainerFluid>
    </BrowserRouter>
  );
}
