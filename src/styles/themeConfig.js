const gridConfig = {
  xs: { columns: 1, gutter: 12 },
  sm: { columns: 2, gutter: 16 },
  md: { columns: 3, gutter: 20 },
};

export default gridConfig;
