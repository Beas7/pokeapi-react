const API_BASE_URL = 'https://pokeapi.co/api/v2/pokemon';

export default API_BASE_URL;
