// @ts-check
import API_BASE_URL from './config';

const customError = {
  error: true,
  name: 'MISSINGNO.',
  id: '????',
  types: [{ type: { name: '????' } }],
  height: '????',
  abilities: [{ ability: { name: '????' } }],
};

/**
 * Generates the url with the corresponding params
 * @param {string} params specific API request
 * @returns {string} url to fetch on
 */
const buildApiRoute = (params) => `${API_BASE_URL}${params.startsWith('?') ? params : `/${params}`}`;

/**
 * Retrieves content from the pokemon API
 * @param {string} params optional params
 * @returns {Promise} fetch result
 */
async function retrieveContent(params = '') {
  try {
    const res = await fetch(buildApiRoute(params));
    const pokemon = await res.json();
    return pokemon;
  } catch (error) {
    return customError;
  }
}

/**
 * Retrieves the 151 pokemon list (1st gen)
 * @returns {Promise} Array of pokemon
 */
const fetchPokemon = async () => retrieveContent('?limit=151');
export default fetchPokemon;
/**
 * Retrieves a pokemon by its name
 * @param {string} pokemonName
 * @returns {Promise<Object>}  Object containing pokemon data
 */
export const fetchPokemonByName = async (pokemonName) => retrieveContent(pokemonName);

/**
 * Retrieves a pokemon animated sprite url
 * @param {string} pokemonName
 * @returns {string} spriteURL
 */
export function getPokemonSprite(pokemonName) {
  return pokemonName === 'MISSINGNO.'
    ? 'https://ih1.redbubble.net/image.1005502373.6773/flat,128x128,075,f-pad,128x128,f8f8f8.jpg'
    : `https://img.pokemondb.net/sprites/black-white/anim/normal/${pokemonName}.gif`;
}
