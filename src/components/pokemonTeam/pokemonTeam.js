import React from 'react';
import { HPBar, Image, LevelLabel, Paragraph, PkmnCard } from './pokemonTeam.styles';
import { getPokemonSprite } from '../../services/pokeapi';
import capitalize from '../../utils/utils';

/**
 * Retrieves a number between 1 and 100
 * @returns {number} number between 1 and 100
 */
function getRandomLevel() {
  return Math.floor(Math.random() * 100 + 1);
}

/**
 * Retrieves a random percentile between 1 and 100
 * @returns {string} percentile string between 1% and 100%
 */
function getRandomHP() {
  return `${getRandomLevel().toString()}%`;
}

/**
 * Renders the current pokemon team (6 favorite pokemon)
 * @param {Array<Object>} pkmnTeam React state
 * @returns {component} React component
 */
export default function PokemonTeam({ pkmnTeam }) {
  return (
    pkmnTeam.length > 0 &&
    pkmnTeam.map((pkmn, i) => (
      <PkmnCard key={pkmn.name}>
        <i>#{i + 1}</i>
        <div>
          <Image src={getPokemonSprite(pkmn.name)} alt='pokemon' />
          <HPBar props={getRandomHP()} />
        </div>
        <div>
          <Paragraph>{capitalize(pkmn.name)}</Paragraph>
          <LevelLabel>lvl. {getRandomLevel()}</LevelLabel>
        </div>
      </PkmnCard>
    ))
  );
}
