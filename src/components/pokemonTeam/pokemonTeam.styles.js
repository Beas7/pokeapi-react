import styled from 'styled-components';
import gridConfig from '../../styles/themeConfig';

export const PkmnCard = styled.div`
  border: 2px solid;
  border-radius: 0 0.4em;
  margin: 8px 0;
  display: flex;
  align-items: center;
  padding: 2px 12px;
  max-width: 380px;
  min-height: 130px;
  background-origin: content-box;
  box-sizing: border-box;
  background: radial-gradient(
    at top right,
    rgba(255, 255, 255, 1) 0%,
    rgba(255, 255, 255, 1) 5%,
    rgba(220, 84, 56, 1) 6%,
    rgba(220, 84, 56, 1) 10%,
    rgba(255, 255, 255, 1) 11%,
    rgba(255, 255, 255, 1) 12%,
    rgba(220, 84, 56, 1) 13%,
    rgba(220, 84, 56, 1) 20%,
    rgba(220, 84, 56, 1) 100%
  );
  background-size: 100% 300%;
  position: relative;
  @media (max-width: 575px) {
    flex: 0 1 calc((100% / ${gridConfig.xs.columns}) - (${gridConfig.xs.gutter} * 1px));
    margin: calc((${gridConfig.xs.gutter} * 1px) / 2);
  }
  @media (min-width: 575px) {
    flex: 0 1 calc((100% / ${gridConfig.sm.columns}) - (${gridConfig.sm.gutter} * 1px));
    margin: calc((${gridConfig.sm.gutter} * 1px) / 2);
  }
  div {
    display: flex;
    flex-flow: row wrap;
    flex: 0 1 30%;
    max-width: 30%;
    justify-content: center;
  }
  > i {
    position: absolute;
    top: 0;
    left: 0;
    color: white;
  }
`;

export const Image = styled.img`
  text-align: center;
  max-width: max-content;
  flex: 0 1 100%;
  margin-left: 8px;
`;

export const Paragraph = styled.span`
  flex: 0 1 100%;
  font-weight: bold;
  text-align: left;
  padding-left: 12px;
  font-size: large;
  color: white;
`;

export const HPBar = styled.div`
  width: 100%;
  margin-top: 8px;
  flex: 0 1 60% !important;
  max-width: 60% !important;
  height: 8px;
  border: 1px solid;
  background: linear-gradient(
    90deg,
    rgba(56, 220, 105, 1) 0%,
    rgba(56, 220, 105, 1) ${(props) => props.props},
    rgba(216, 92, 61, 0) ${(props) => props.props},
    rgba(216, 92, 61, 0) 100%
  );
`;

export const LevelLabel = styled.small`
  flex: 0 1 100%;
  color: white;
  text-align: center;
  margin-top: 8px;
`;
