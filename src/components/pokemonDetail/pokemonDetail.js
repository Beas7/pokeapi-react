import React, { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import { fetchPokemonByName, getPokemonSprite } from '../../services/pokeapi';
import capitalize from '../../utils/utils';
import Loader from '../loader/loader';
import { AbilitiesList, Close, Container, DescriptionLine, Image, PkmnCard, PkmnName, Wrapper } from './pokemonDetail.styles';

/**
 * Renders a pokemon details component
 * @returns {component} React component
 */
export default function PokemonDetail() {
  const { pokemonName } = useParams();
  const [pokemon, setPokemon] = useState([]);
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    fetchPokemonByName(pokemonName).then((pokemonData) => {
      setPokemon(pokemonData);
      setLoaded(true);
    });
  }, []);

  return (
    <Container>
      {!loaded ? (
        <Loader />
      ) : (
        pokemon && (
          <Wrapper>
            <PkmnCard>
              <Close>
                <Link to='/pokemon'>&times;</Link>
              </Close>
              <Image src={getPokemonSprite(pokemon.name)} alt='pokemon' />
              <PkmnName>{capitalize(pokemon.name)}</PkmnName>
              <DescriptionLine>ID: {pokemon.id}</DescriptionLine>
              <DescriptionLine>
                Type: {pokemon.types.map((type, i) => `${type.type.name}${i + 1 === pokemon.types.length ? '' : ','} `)}
              </DescriptionLine>
              <DescriptionLine>Height: {pokemon.height}</DescriptionLine>
              <DescriptionLine>Abilities:</DescriptionLine>
              <AbilitiesList>
                {pokemon.abilities.map((ability) => (
                  <li key={ability.ability.name}>{ability.ability.name}</li>
                ))}
              </AbilitiesList>
            </PkmnCard>
          </Wrapper>
        )
      )}
    </Container>
  );
}
