import styled from 'styled-components';

export const Container = styled.section`
  max-width: 1200px;
  margin: 0 auto;
  padding: 12px;
`;
export const Image = styled.img`
  flex: 0 1 100%;
  text-align: center;
  max-width: max-content;
  margin-top: 24px;
`;
export const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 40px;
`;
export const PkmnCard = styled.div`
  border: 1px solid;
  padding: 8px 24px;
  width: 300px;
  display: flex;
  flex-flow: row wrap;
  justify-content: center;
  background-color: white;
  position: relative;
  img,
  p,
  ul {
    flex: 0 1 100%;
  }
`;
export const PkmnName = styled.p`
  text-align: center;
  font-weight: bold;
`;
export const AbilitiesList = styled.ul`
  margin-top: 0;
`;
export const DescriptionLine = styled.p`
  margin-bottom: 0;
`;
export const Close = styled.div`
  position: absolute;
  top: 4px;
  right: 6px;
  a {
    color: black;
    text-decoration: none;
    font-size: larger;
  }
`;
