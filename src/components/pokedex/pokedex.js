import React, { useEffect, useState } from 'react';
import PropTypes, { objectOf } from 'prop-types';
import capitalize from '../../utils/utils';
import fetchPokemon, { getPokemonSprite } from '../../services/pokeapi';
import Loader from '../loader/loader';
import { Pokegallery, Paragraph, Image, LS, Star, SearchBox } from './pokedex.styles';
/** @type {NodeJS.Timeout} */
let debounceTimer;

/**
 * Returns true if a pokemon is already starred, else returns false
 * @param {Array<Object>} pkmnTeam
 * @param {string} name
 * @returns {boolean}
 */
const isFavorite = (pkmnTeam, name) => pkmnTeam.filter((pkmn) => pkmn.name === name).length > 0;

/**
 * Adds or substract the pokemon from the pokemonTeam array.
 * It shows an alert message if the maximum pokemon in the team is reached.
 * @param {Event} e
 * @param {Object} pkmn pokemon object
 * @param {Array<Object>} pkmnTeam current pokemonTeam
 * @param {Function} setPkmnTeam pokemonTeam setter
 */
const toggleFavoritePokemon = (e, pkmn, pkmnTeam, setPkmnTeam) => {
  e.preventDefault();

  if (pkmnTeam.length < 6 && !isFavorite(pkmnTeam, pkmn.name)) {
    setPkmnTeam((prevPkmnTeam) => [...prevPkmnTeam, { name: pkmn.name }]);
  } else if (isFavorite(pkmnTeam, pkmn.name)) {
    setPkmnTeam((prevPkmnTeam) => prevPkmnTeam.filter((pk) => pk.name !== pkmn.name));
  } else {
    alert('Your team is complete!\nYou are not allowed to bring more than 6 pokemon with you.');
  }
};

/**
 * Filters the content of the pokemon grid
 * @param {Object} pokemon pokemon
 * @param {string} searchString string to filter the pokemon with
 * @returns {Array<Object>} filtered pokemon list
 */
const filterContent = (pokemon, searchString) => pokemon.filter((pkmn) => pkmn.name.includes(searchString));

/**
 * Debounces the search eventlistener and functionallity
 * @param {Function} callback Function to execute after the time
 * @param {number} time Time in ms to spare between callback executions
 */
const debounce = (callback, time) => {
  window.clearTimeout(debounceTimer);
  debounceTimer = window.setTimeout(callback, time);
};

/**
 * Renders a list of pokemon cards
 * @param {Object} pkmnTeam React state
 * @param {Function} setPkmnTeam React set state
 * @returns {component} React component
 */
export default function Pokedex({ pkmnTeam, setPkmnTeam }) {
  const [pokemon, setPokemon] = useState([]);
  const [filtered, setFiltered] = useState([]);
  const [loaded, setLoaded] = useState(false);
  const [searchString, setSearchString] = useState('');

  useEffect(() => {
    fetchPokemon().then((pokemonData) => {
      if (!pokemonData.error) {
        setPokemon(pokemonData.results);
        setFiltered(pokemon);
      }
      setLoaded(true);
    });
  }, []);

  useEffect(() => {
    setFiltered(filterContent(pokemon, searchString));
  }, [pokemon, searchString]);

  return (
    <>
      <SearchBox
        type='text'
        placeholder='Start typing to search'
        onChange={(e) => {
          debounce(setSearchString(e.target.value), 250);
        }}
      />
      <Pokegallery>
        {!loaded ? (
          <Loader />
        ) : (
          (filtered.length > 0 &&
            filtered.map((pkmn) => (
              <LS.CardLinkItem to={`/pokemon/${pkmn.name}`} key={pkmn.name}>
                <Star active={isFavorite(pkmnTeam, pkmn.name)} onClick={(e) => toggleFavoritePokemon(e, pkmn, pkmnTeam, setPkmnTeam)} />
                <Image src={getPokemonSprite(pkmn.name)} alt={`pokemon_${pkmn.name}`} />
                <Paragraph align='center'>{capitalize(pkmn.name)}</Paragraph>
              </LS.CardLinkItem>
            ))) ||
          (pokemon.length > 0 && <Paragraph align='left'>No results found with this filter.</Paragraph>) || (
            <Paragraph align='center'>Something went wrong, try again later.</Paragraph>
          )
        )}
      </Pokegallery>
    </>
  );
}

Pokedex.propTypes = {
  pkmnTeam: PropTypes.arrayOf(objectOf(PropTypes.string)),
  setPkmnTeam: PropTypes.func.isRequired,
};
Pokedex.defaultProps = {
  pkmnTeam: [{ name: 'abra' }],
};
/**
 * Creates an index to filter
 * @param {*} pokemon
 * @returns
 */
// const indexPokemon = (pokemonArray) => pokemonArray.reduce((acc, el) => ({ ...acc, [el.name]: el }), {});
