import styled from 'styled-components';

const Container = styled.section`
  max-width: 1200px;
  margin: 0 auto;
  padding: 12px;
`;

export default Container;
