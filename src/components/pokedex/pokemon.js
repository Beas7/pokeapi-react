import React from 'react';
import PropTypes, { objectOf } from 'prop-types';
import Hero from '../header/header';
import Pokedex from './pokedex';
import Container from './pokemon.styles';

/**
 * Wrapper component. Renders the Hero and the PokemonList
 * @param {Object} pkmnTeam React state
 * @param {Function} setPkmnTeam React set state
 * @returns {component} React component
 */
export default function Pokemon({ pkmnTeam, setPkmnTeam }) {
  return (
    <Container>
      <Hero />
      <Pokedex pkmnTeam={pkmnTeam} setPkmnTeam={setPkmnTeam} />
    </Container>
  );
}

Pokemon.propTypes = {
  pkmnTeam: PropTypes.arrayOf(objectOf(PropTypes.string)),
  setPkmnTeam: PropTypes.func.isRequired,
};
Pokemon.defaultProps = {
  pkmnTeam: [{ name: 'abra' }],
};
