import { Link } from 'react-router-dom';
import styled from 'styled-components';
import gridConfig from '../../styles/themeConfig';

export const Pokegallery = styled.div`
  display: flex;
  flex-flow: row wrap;
`;
export const Paragraph = styled.p`
  font-weight: bold;
  flex: 0 1 100%;
  text-align: ${(props) => props.align};
`;
export const Image = styled.img`
  flex: 0 1 100%;
  text-align: center;
  max-width: max-content;
`;
export const LS = {};
LS.CardLinkItem = styled(Link)`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  align-items: center;
  box-shadow: 0 0 7px #333333aa;
  border-radius: 0.4em;
  min-height: 220px;
  padding: 24px 0 8px 0;
  position: relative;
  text-decoration: none;
  color: #212121;
  z-index: 1;
  cursor: pointer;
  &:not(:hover) {
    background-color: #fff;
  }
  &:hover {
    background-color: lightgray;
  }
  @media (max-width: 425px) {
    flex: 0 1 calc((100% / ${gridConfig.xs.columns}) - (${gridConfig.xs.gutter} * 1px));
    margin: calc((${gridConfig.xs.gutter} * 1px) / 2);
  }
  @media (min-width: 425px) and (max-width: 992px) {
    flex: 0 1 calc((100% / ${gridConfig.sm.columns}) - (${gridConfig.sm.gutter} * 1px));
    margin: calc((${gridConfig.sm.gutter} * 1px) / 2);
  }
  @media (min-width: 991px) {
    flex: 0 1 calc((100% / ${gridConfig.md.columns}) - (${gridConfig.md.gutter} * 1px));
    margin: calc((${gridConfig.md.gutter} * 1px) / 2);
  }
`;

export const Star = styled.i`
  position: absolute;
  top: 8px;
  right: 8px;
  display: inline-block;
  width: 0;
  height: 0;
  margin-left: 0.9em;
  margin-right: 0.9em;
  margin-bottom: 1.2em;
  border-right: 0.3em solid transparent;
  border-bottom: 0.7em solid ${(props) => (props.active ? '#fc0' : '#f0f0f0')};
  border-left: 0.3em solid transparent;
  font-size: 20px;
  &:before,
  &:after {
    content: '';
    display: block;
    width: 0;
    height: 0;
    position: absolute;
    top: 0.6em;
    left: -1em;
    border-right: 1em solid transparent;
    border-bottom: 0.7em solid ${(props) => (props.active ? '#fc0' : '#f0f0f0')};
    border-left: 1em solid transparent;
    transform: rotate(-35deg);
  }
  &:after {
    transform: rotate(35deg);
  }
`;
export const SearchBox = styled.input`
  display: block;
  padding: 4px;
  margin: calc(${gridConfig.md.gutter} * 1px / 2);
  border: none;
  border-radius: 2px;
  outline: none;
  box-shadow: 0 0 5px #212121ee;
`;
