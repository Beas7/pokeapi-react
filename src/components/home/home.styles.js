import styled from 'styled-components';

export const Container = styled.section`
  max-width: 1200px;
  margin: 0 auto;
  padding: 12px;
`;

export const Wrapper = styled.div`
  display: flex;
  flex-flow: row wrap;
`;
