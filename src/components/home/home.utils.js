import { Link } from 'react-router-dom';

/**
 * Returns a message notifying that the current team is empty
 * @returns {string} empty team message
 */
const getEmptyTeamMessage = () => (
  <p>
    It seems that you didn&apos;t build your team yet, start choosing pokemon <Link to='/pokemon'>here.</Link>{' '}
  </p>
);

/**
 * Returns a message notifying that the current team is full
 * @returns {string} full team message
 */
const getCompleteTeamMessage = () => <p>Team complete!</p>;

/**
 * Returns a message notifying that the current team is not complete
 * @returns {string} incomplete team message
 */
const getIncompleteTeamMessage = (pokemonAmount) => (
  <span>
    You currently have {pokemonAmount} pokemon in your team. Remember that you can choose up to 6 of your favorite pokemon to build your team!{' '}
    <Link to='/pokemon'>add more.</Link>
  </span>
);

/**
 * Retrieves a conditional message based on the number of pokemon added to the current team
 * @param {number} numberOfPokemon
 * @returns {string} message
 */
export function getTeamMessage(numberOfPokemon = 0) {
  let message;
  if (numberOfPokemon < 1) {
    message = getEmptyTeamMessage();
  } else if (numberOfPokemon === 6) {
    message = getCompleteTeamMessage();
  } else {
    message = getIncompleteTeamMessage(numberOfPokemon);
  }
  return message;
}
