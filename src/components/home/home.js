import React from 'react';
import PropTypes, { objectOf } from 'prop-types';
import { Container, Wrapper } from './home.styles';
import PokemonTeam from '../pokemonTeam/pokemonTeam';
import { getTeamMessage } from './home.utils';

/**
 * Renders the home view of the webApp
 * @param {Array<Object>} pkmnTeam React state
 * @returns {component} React component
 */
export default function Home({ pkmnTeam }) {
  return (
    <Container>
      <h2>Your Pokemon Team</h2>
      {getTeamMessage(pkmnTeam.length)}
      <Wrapper>
        <PokemonTeam pkmnTeam={pkmnTeam} />
      </Wrapper>
    </Container>
  );
}

Home.propTypes = {
  pkmnTeam: PropTypes.arrayOf(objectOf(PropTypes.string)),
};
Home.defaultProps = {
  pkmnTeam: [{ name: 'abra' }],
};
