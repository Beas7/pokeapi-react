import React from 'react';
import { Link } from 'react-router-dom';
import { Logo, NavBar, Wrapper } from './nav.styles';
import PokeballLogo from '../../public/images/LogoPokeball.png';

/**
 * Renders the navigation bar with logo
 * @returns {component} React component
 */
export default function Nav() {
  return (
    <NavBar>
      <Wrapper>
        <Logo src={PokeballLogo} alt='pokemon_logo' />
        <Link to='/'>Home</Link>
        <Link to='/pokemon'>Pokemon</Link>
      </Wrapper>
    </NavBar>
  );
}
