import styled from 'styled-components';

const theme = {
  fg: 'white',
  bg: 'lightgray',
};

export const NavBar = styled.nav`
  width: 100%;
  box-shadow: 0 1px 7px #21212199;
  position: fixed;
  background-color: white;
  max-height: 80px;
  z-index: 9;
  a {
    display: flex;
    align-items: center;
    padding: 12px;
    text-decoration: none;
    color: #212121;
    background-color: ${theme.fg};
    &:hover {
      background-color: ${theme.bg};
    }
  }
`;

export const Logo = styled.img`
  display: block;
  max-height: 60px;
  width: auto;
  padding: 4px;
  @media (max-width: 475px) {
    margin-right: 12px;
  }
  @media (min-width: 475px) {
    margin-right: 36px;
  }
`;

export const Wrapper = styled.div`
  max-width: 1200px;
  margin: 0 auto;
  display: flex;
  flex-flow: row nowrap;
  justify-content: flex-start;
`;
