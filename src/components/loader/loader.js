import React from 'react';
import { LoaderContainer, Spinner } from './loader.styles';

/**
 * Renders a spinner while fetching data
 * @returns {component} React component
 */
export default function Loader() {
  return (
    <LoaderContainer>
      <Spinner />
    </LoaderContainer>
  );
}
