import styled from 'styled-components';

export const LoaderContainer = styled.div`
  background: #f5f5f5;
  width: 100vw;
  max-width: 100vw;
  height: 100vh;
  z-index: 99999;
  position: fixed;
  top: 50;
  right: 0;
  left: 0;
`;

export const Spinner = styled.div`
  border-radius: 50%;
  color: #f0f0f0;
  font-size: 11px;
  text-indent: -99999em;
  margin: 55px auto;
  position: relative;
  width: 10em;
  height: 10em;
  box-shadow: inset 0 0 0 1em;
  -webkit-transform: translateZ(0);
  -ms-transform: translateZ(0);
  transform: translateZ(0);
  &:before,
  &:after {
    border-radius: 50%;
    position: absolute;
    content: '';
  }
  &:before {
    width: 5.2em;
    height: 10.2em;
    background: #465ca8;
    border-radius: 10.2em 0 0 10.2em;
    top: -0.1em;
    left: -0.1em;
    -webkit-transform-origin: 5.1em 5.1em;
    transform-origin: 5.1em 5.1em;
    -webkit-animation: load 2s infinite ease 1.5s;
    animation: load 2s infinite ease 1.5s;
    box-shadow: 0 0 5px #333;
  }
  &:after {
    width: 5.2em;
    height: 10.2em;
    background: #f3ce21;
    border-radius: 0 10.2em 10.2em 0;
    top: -0.1em;
    left: 4.9em;
    -webkit-transform-origin: 0.1em 5.1em;
    transform-origin: 0.1em 5.1em;
    -webkit-animation: load 2s infinite ease;
    animation: load 2s infinite ease;
    box-shadow: 0 0 5px #333;
  }
  @-webkit-keyframes load {
    0% {
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
    }
    100% {
      -webkit-transform: rotate(360deg);
      transform: rotate(360deg);
    }
  }
  @keyframes load {
    0% {
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
    }
    100% {
      -webkit-transform: rotate(360deg);
      transform: rotate(360deg);
    }
  }
`;
