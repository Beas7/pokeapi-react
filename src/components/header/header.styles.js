import styled from 'styled-components';

export const Header = styled.header`
  margin: 0 auto;
  max-width: 600px;
  padding: 8px 24px 36px;
`;

export const Logo = styled.img`
  max-width: 100%;
  margin-bottom: 40px;
`;

export const Paragraph = styled.p`
  flex: 0 1 100%;
  text-align: center;
`;

export const Subtitle = styled.p`
  font-size: large;
  font-weight: 600;
  flex: 0 1 100%;
  text-align: center;
`;
