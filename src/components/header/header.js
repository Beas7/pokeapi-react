import React from 'react';
import { Header, Logo, Paragraph, Subtitle } from './header.styles';
import internationalPokemonLogo from '../../public/images/International_Pokemon_Logo.png';

/**
 * Renders the hero: Pokemon official logo and subtitle
 * @returns {component} React component
 */
export default function Hero() {
  return (
    <Header>
      <Logo src={internationalPokemonLogo} alt='pokemon_logo' />
      <Subtitle>Generation 1</Subtitle>
      <Paragraph>151 pokemon</Paragraph>
    </Header>
  );
}
