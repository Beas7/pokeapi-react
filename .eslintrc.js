module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: ['plugin:react/recommended', 'airbnb'],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 'latest',
    sourceType: 'module',
  },
  plugins: ['react'],
  rules: {
    'react/jsx-filename-extension': [1, { extensions: ['.js', '.jsx'] }],
    'jsx-quotes': [2, 'prefer-single'],
    'operator-linebreak': [0, 'off'],
    'object-curly-newline': ['off'],
    'react/jsx-one-expression-per-line': 'off',
    'max-len': [1, 150],
    'no-alert': 'off',
  },
};
